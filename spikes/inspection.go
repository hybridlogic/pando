package main

import (
	"errors"
	"fmt"
	"reflect"
)

func RandomCommand(foo string, bar int) {
	fmt.Println("from random command:", foo, bar)
}

func Call(m map[string]interface{}, name string, params ... interface{}) (result []reflect.Value, err error) {
	f := reflect.ValueOf(m[name])
	if len(params) != f.Type().NumIn() {
		err = errors.New("The number of params is not adapted.")
		return
	}
	in := make([]reflect.Value, len(params))
	for k, param := range params {
		in[k] = reflect.ValueOf(param)
	}
	result = f.Call(in)
	return
}

func main() {
	fmt.Println(reflect.TypeOf(RandomCommand).String())

	f := reflect.ValueOf(RandomCommand)  //could be a map with key string and blah
	fmt.Println(f)

	in := make([]reflect.Value, f.Type().NumIn())
	params := make([]interface{}, f.Type().NumIn())

	fmt.Println("params", params)

	fmt.Println("method type num in:", f.Type().NumIn())
	for i := 0; i < f.Type().NumIn(); i++ {
		t := f.Type().In(i)
		fmt.Println(i, t, t.Elem().Name())
		//object := objects[t]
		//fmt.Println(i, "->", object)
		//in[i] = reflect.ValueOf(object)
	}


	//if len(params) != f.Type().NumIn() {
	//	err = errors.New("The number of params is not adapted.")
	//	return
	//}

	//in := make([]reflect.Value, len(params))
	for k, param := range params {
		in[k] = reflect.ValueOf(param)
	}
	//result = f.Call(in)


	//RandomCommand()
}