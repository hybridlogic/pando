package context

import (
	"encoding/json"
	"fmt"
	"sync"

	"pando/pkg/config"
	"pando/pkg/node"
	"pando/pkg/node/ntypes"
)

// The context is all the thing this node or client needs to know about.
type ContextStore struct {
	config       *config.Config
	node         node.Node
	nodes        map[ntypes.NodeID]node.Node       //todo: make a sync.Map?
	nodesByAddr  map[ntypes.NodeAddr]ntypes.NodeID //todo: make a sync.Map?
	sectorsOther sync.Map                          //map[SectorID]*Sector
	sectorsMine  sync.Map                          //map[SectorID]*Sector
}

func NewContext() *ContextStore {
	conf := config.LoadConfig()
	//n, _ := node.NewNode(conf.Advertise)
	ctx := ContextStore{
		config: conf,
		//node:        n,
		nodes:       make(map[ntypes.NodeID]node.Node),
		nodesByAddr: make(map[ntypes.NodeAddr]ntypes.NodeID),
	}
	return &ctx
}

func (ctx *ContextStore) String() string {
	m, _ := json.Marshal(ctx)
	return fmt.Sprint(string(m))
}

func (ctx *ContextStore) Config() *config.Config {
	return ctx.config
}

//func (ctx *ContextStore) State() NodeState {
//	return State.Alive
//}
//
func (ctx *ContextStore) ContextNode() node.Node {
	return ctx.node
}

func (ctx *ContextStore) Call(method string, request interface{}, response interface{}) error {
	ctx.Nodes()[0].Send(method, request, response)
	return nil
}

//
//func (ctx *ContextStore) Sector(id SectorID) *Sector {
//	var (
//		sector *Sector
//		ok     bool
//	)
//	seci, found := ctx.sectorsMine.Load(id)
//	if !found {
//		sector, ok = ctx.findSector(id)
//	} else {
//		sector, ok = seci.(*Sector)
//		if !ok {
//			logger.Error("Could not load", id)
//		}
//	}
//	return sector
//}
//
//func (ctx *ContextStore) mineSector(id SectorID) (*Sector, bool) {
//	seci, ok := ctx.sectorsMine.Load(id)
//	if ok {
//		sector, ok := seci.(*Sector)
//		if ok {
//			return sector, ok
//		}
//	}
//	return nil, ok
//}
//
//func (ctx *ContextStore) otherSector(id SectorID) (*Sector, bool) {
//	seci, ok := ctx.sectorsOther.Load(id)
//	if ok {
//		sector, ok := seci.(*Sector)
//		if ok {
//			return sector, ok
//		}
//	}
//	return nil, ok
//}
//
//func (ctx *ContextStore) findSector(id SectorID) (*Sector, bool) {
//	for _, n := range ctx.Nodes() {
//		sector, err := n.SectorInfo(id)
//		if sector != nil && err == nil {
//			return sector, true
//		}
//	}
//	return nil, false
//}
//
//func (ctx *ContextStore) createSector(id SectorID) (*Sector, bool) {
//	// todo: node select not random
//	// todo: lock or wait group?
//	sector := &Sector{ID: id}
//	for i := 0; i < min(3, len(ctx.nodes)); i++ {
//		sector.Nodes = append(sector.Nodes, ctx.Nodes()[rand.Intn(len(ctx.nodes)-1)].ID)
//	}
//	sector.Master = sector.Nodes[0]
//	//tel the nodes they are in charge
//	for _, id := range sector.Nodes {
//		ctx.NodeByID(id).RegisterSector(sector)
//	}
//	return sector, true
//}
//
////Finds all the nodes in the cluster from other nodes
//func (ctx *ContextStore) FindNodes(addr NodeAddr) {
//	log.Println("Finding new node: ", addr)
//	var node *Node
//	node = ctx.NodeByAddr(addr)
//	if node != nil {
//		log.Println("I already know about:", node)
//		return
//	} else {
//		node, _ = NewNode(addr)
//
//		response, err := node.Register(ctx.ContextNode())
//		if err == nil {
//			//node.ID = response.ID
//			ctx.AddNode(node)
//		}
//		log.Println(response.Node, "knows about", response.Nodes)
//		for _, otherNode := range response.Nodes {
//			ctx.FindNodes(otherNode.Addr)
//		}
//	}
//}
//
func (ctx *ContextStore) NodeByAddr(addr ntypes.NodeAddr) node.Node {
	return ctx.nodes[ctx.nodesByAddr[addr]]
}

//
func (ctx *ContextStore) NodeByID(id ntypes.NodeID) node.Node {
	return ctx.nodes[id]
}

//
func (ctx *ContextStore) AddNode(node node.Node) {
	ctx.nodes[node.ID()] = node
	ctx.nodesByAddr[node.Addr()] = node.ID()
}

//
//func (ctx *ContextStore) RemoveNode(node *Node) {
//	//todo: implement
//}
//
func (ctx *ContextStore) Nodes() []node.Node {
	var nodes []node.Node
	for _, node := range ctx.nodes {
		nodes = append(nodes, node)
	}
	return nodes
}

//
//func (ctx *ContextStore) ListNodes() {
//	for k, node := range ctx.nodes {
//		fmt.Println("k:", k, "v:", node)
//	}
//}
