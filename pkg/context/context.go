package context

import (
	"pando/pkg/config"
	"pando/pkg/node"
	"pando/pkg/node/ntypes"
)

type Context interface {
	//ID() utils.UUID
	Config() *config.Config
	ContextNode() node.Node
	Nodes() []node.Node
	NodeByAddr(ntypes.NodeAddr) node.Node
	AddNode(node.Node)
	Call(string, interface{}, interface{}) error
}

//// The context is all the thing this node or client needs to know about.
//type Context struct {
//	config       *config.Config
//	Node         *node.Node
//	nodes        map[node.NodeID]*node.Node
//	nodesByAddr  map[node.NodeAddr]node.NodeID
//	sectorsOther sync.Map //map[SectorID]*Sector
//	sectorsMine  sync.Map //map[SectorID]*Sector
//}
//
//func NewContext() *Context {
//	conf := config.LoadConfig()
//	//n, _ := node.NewNode(conf.Advertise)
//	context := Context{
//		config:      conf,
//		//Node:        n,
//		//nodes:       make(map[node.NodeID]*node.Node),
//		//nodesByAddr: make(map[node.NodeAddr]node.NodeID),
//	}
//	return &context
//}
//
//func (ctx *Context) String() string {
//	m, _ := json.Marshal(ctx)
//	return fmt.Sprint(string(m))
//}

//func (ctx *Context) State() NodeState {
//	return State.Alive
//}
//
//func (ctx *Context) ContextNode() *Node {
//	return ctx.Node
//}
//
//func (ctx *Context) Sector(id SectorID) *Sector {
//	var (
//		sector *Sector
//		ok     bool
//	)
//	seci, found := ctx.sectorsMine.Load(id)
//	if !found {
//		sector, ok = ctx.findSector(id)
//	} else {
//		sector, ok = seci.(*Sector)
//		if !ok {
//			logger.Error("Could not load", id)
//		}
//	}
//	return sector
//}
//
//func (ctx *Context) mineSector(id SectorID) (*Sector, bool) {
//	seci, ok := ctx.sectorsMine.Load(id)
//	if ok {
//		sector, ok := seci.(*Sector)
//		if ok {
//			return sector, ok
//		}
//	}
//	return nil, ok
//}
//
//func (ctx *Context) otherSector(id SectorID) (*Sector, bool) {
//	seci, ok := ctx.sectorsOther.Load(id)
//	if ok {
//		sector, ok := seci.(*Sector)
//		if ok {
//			return sector, ok
//		}
//	}
//	return nil, ok
//}
//
//func (ctx *Context) findSector(id SectorID) (*Sector, bool) {
//	for _, n := range ctx.Nodes() {
//		sector, err := n.SectorInfo(id)
//		if sector != nil && err == nil {
//			return sector, true
//		}
//	}
//	return nil, false
//}
//
//func (ctx *Context) createSector(id SectorID) (*Sector, bool) {
//	// todo: node select not random
//	// todo: lock or wait group?
//	sector := &Sector{ID: id}
//	for i := 0; i < min(3, len(ctx.nodes)); i++ {
//		sector.Nodes = append(sector.Nodes, ctx.Nodes()[rand.Intn(len(ctx.nodes)-1)].ID)
//	}
//	sector.Master = sector.Nodes[0]
//	//tel the nodes they are in charge
//	for _, id := range sector.Nodes {
//		ctx.NodeByID(id).RegisterSector(sector)
//	}
//	return sector, true
//}
//
////Finds all the nodes in the cluster from other nodes
//func (ctx *Context) FindNodes(addr NodeAddr) {
//	log.Println("Finding new node: ", addr)
//	var node *Node
//	node = ctx.NodeByAddr(addr)
//	if node != nil {
//		log.Println("I already know about:", node)
//		return
//	} else {
//		node, _ = NewNode(addr)
//
//		response, err := node.Register(ctx.ContextNode())
//		if err == nil {
//			//node.ID = response.ID
//			ctx.AddNode(node)
//		}
//		log.Println(response.Node, "knows about", response.Nodes)
//		for _, otherNode := range response.Nodes {
//			ctx.FindNodes(otherNode.Addr)
//		}
//	}
//}
//
//func (ctx *Context) NodeByAddr(addr NodeAddr) *Node {
//	return ctx.nodes[ctx.nodesByAddr[addr]]
//}
//
//func (ctx *Context) NodeByID(id NodeID) *Node {
//	return ctx.nodes[id]
//}
//
//func (ctx *Context) AddNode(node *Node) {
//	ctx.nodes[node.ID] = node
//	ctx.nodesByAddr[node.Addr] = node.ID
//}
//
//func (ctx *Context) RemoveNode(node *Node) {
//	//todo: implement
//}
//
//func (ctx *Context) Nodes() []*Node {
//	var nodes []*Node
//	for _, node := range ctx.nodes {
//		nodes = append(nodes, node)
//	}
//	return nodes
//}
//
//func (ctx *Context) ListNodes() {
//	for k, node := range ctx.nodes {
//		fmt.Println("k:", k, "v:", node)
//	}
//}
