package context

import (
	"pando/pkg/node/ntypes"
)

type SectorID = uint32

// Sector is the mini cluster inside of pando.
// Sectors are replicated across the entire pando cluster.[32]byte
type Sector struct {
	ID     SectorID
	Master ntypes.NodeID
	Nodes  []ntypes.NodeID
}
