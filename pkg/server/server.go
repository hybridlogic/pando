package server

import (
	"net"
	"pando/pkg/context"
	"pando/pkg/go/net/rpc"
	"pando/pkg/logger"
)

type Server struct {
	context   context.Context
	listener  *net.Listener
	rpcServer *rpc.Server
	running   bool
}

func NewServer(context context.Context) *Server {
	logger.Debug("Building new server")
	server := Server{context: context}
	return &server
}

func (srv *Server) Serve() {
	//todo: split things out
	logger.Info("Starting Server... ") // todo: add server info
	logger.Info(srv)
	l, err := net.Listen("tcp", srv.context.Config().Address)
	logger.FatalIf(err)
	// todo: sync Once function?
	//request := new(Server)
	rpcSrv := rpc.NewServer(srv.context)
	//err = rpcSrv.Register(srv)
	//logger.FatalIf(err)

	//fileRPC := pando.NewFileRPC(ctx)
	//err = rpcSrv.Register(fileRPC)
	//logger.FatalIf(err)

	nodeRPC := NewNodeRPC(srv.context)
	err = rpcSrv.Register(nodeRPC)
	logger.FatalIf(err)

	go func() {
		defer l.Close()
		rpcSrv.Accept(l)
	}()
}
