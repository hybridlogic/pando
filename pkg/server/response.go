package server

import (
	"pando/pkg/node"
	"pando/pkg/node/ntypes"
	"pando/pkg/utils"
	"time"
)

//type Response interface {
//	ID() UUID
//	Exec(ctx *Context) error
//}

type Response struct {
	ID       utils.UUID
	Time     time.Time
	Response string
	Node     node.Node
}

func requestToResponse(request Request,  respNode node.Node) Response {
	return Response{request.ID, time.Now(), request.Request, respNode}
}

type AliveResponse struct {
	Response
	state ntypes.NodeState
}

func NewAliveResponse(state ntypes.NodeState) *AliveResponse {
	response := AliveResponse{Response{Response: "alive"}, state}
	return &response
}


type NodeListResponse struct {
	Response
	Nodes []*node.Node
}