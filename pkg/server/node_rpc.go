package server

import (
	"fmt"
	"math"
	"math/rand"
	"pando/pkg/context"
	"pando/pkg/logger"
	nodeStore "pando/internal/pkg/node"
	"pando/pkg/node"
)

type NodeRPC struct {
	context context.Context
}

func NewNodeRPC(ctx context.Context) *NodeRPC {
	return &NodeRPC{context:ctx}
}

type RegisterRequest struct {
	Request
	//ID   ntypes.NodeID
	//Addr ntypes.NodeAddr
	//todo: security?
}

type RegisterResponse struct {
	Response
}

// Registers a new node in the cluster
func (rpc *NodeRPC) Register(register *RegisterRequest, response *RegisterResponse) error {
	logger.Debug("Registering", register.Node)
	var n node.Node
	n = rpc.context.NodeByAddr(register.Node.Addr())
	//todo: also check the node is, it may connect with a different address
	if n == nil {
		n, _ = nodeStore.NewNode(register.Node.Addr())
		rpc.context.AddNode(n)
	}
	//requestToResponse(register, response, srv.context.ContextNode())
	response.Response = requestToResponse(register.Request, rpc.context.ContextNode())
	//response.Nodes = rpc.context.Nodes()
	return nil
}

//
//func (rpc *NodeRPC) Heartbeat(request *HeartbeatRequest, response *RegisterResponse) error {
//	logger.Debug("heartbeat from", request.Node)
//	return nil
//}
//
//func (rpc *NodeRPC) NodeList(request *NodeListRequest, response *NodeListResponse) error {
//	logger.Debug("node list request from", request.Node)
//	response.Response = requestToResponse(request.Request, srv.context.ContextNode())
//	response.Nodes = srv.context.Nodes()
//	return nil
//}


type SystemLoadRequest struct {
	Request
}

type SystemLoadResponse struct {
	Response
	Processor float64
	Memory    float64
	Storage   float64
	Network   float64
	Resource  float64
}

func (resp *SystemLoadResponse) String() string {
	return fmt.Sprint("CPU: ", resp.Processor)
}

func (resp *SystemLoadResponse) Load() float64 {
	return math.Pow(resp.Processor, resp.Processor * 2) +
		math.Pow(resp.Memory, resp.Memory * 2) +
		math.Pow(resp.Storage, resp.Storage * 2) +
		math.Pow(resp.Network, resp.Network * 2) +
		math.Pow(resp.Resource, resp.Resource * 2)
}

func (rpc *NodeRPC) SystemLoad(request *SystemLoadRequest, response *SystemLoadResponse) error {
	logger.Debug("wanted to know load information")
	response.Processor = rand.Float64()
	response.Memory = rand.Float64()
	response.Network = rand.Float64()
	response.Storage = rand.Float64()
	response.Resource = rand.Float64()
	return nil
}
