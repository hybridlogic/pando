package server

import (
	"pando/pkg/node"
	"pando/pkg/utils"
	"time"
)

type RequestInterface interface {
	ID() utils.UUID
}

type Request struct {
	ID      utils.UUID
	Time    time.Time
	Request string
	Node    node.Node
}

func (req *Request) RequestID () utils.UUID {
	return req.ID
}

func requestArgs(request string, reqNode node.Node) Request {
	uuid, _ := utils.NewUUID()
	//logError(err)
	return Request{ID: uuid, Time: time.Now(), Request: request, Node: reqNode}
}
