package server

import (
	"pando/pkg/context"
)

//const PandoClusterResourceType ResourceType = "PANDO_CLUSTER"

type ClusterRPC struct {
	context context.Context
}

func NewClusterRPC(ctx context.Context) *ClusterRPC {
	rpc := ClusterRPC{context: ctx}
	return &rpc
}

type SectorInfoRequest struct {
	Request
	SectorID context.SectorID
}

type SectorInfoResponse struct {
	Response
	context.Sector
}



//func (rpc *ClusterRPC) SectorInfo(request *SectorInfoRequest, response *SectorInfoResponse) error {
//	log.Println("Asked to find sector", request.SectorID)
//	response.Sector.Nodes = []ntypes.NodeID{rpc.context.Node.ID}
//	response.Sector.Master = rpc.context.ContextNode().ID
//	return nil
//}
//
//func (rpc *ClusterRPC) SystemLoad(request *SystemLoadRequest, response *SystemLoadResponse) error {
//	response.Processor = rand.Float32()
//	response.Memory = rand.Float32()
//	response.Network = rand.Float32()
//	response.Storage = rand.Float32()
//	response.Resource = rand.Float32()
//	return nil
//}
//
//func (n *node.Node) SectorInfo(id context.SectorID) (*nodeSector, error) {
//	request := SectorInfoRequest{requestArgs("SECTORINFO", n), id}
//	response := SectorInfoResponse{}
//	err := n.Send("ClusterRPC.SectorInfo", &request, &response)
//	// do something
//	return &response.Sector, err
//}
//
//func (n *Node) SystemLoad() (*SystemLoadResponse, error) {
//	request := SystemLoadRequest{requestArgs("SYSTEMLOAD", n),}
//	response := SystemLoadResponse{}
//	err := n.Send("ClusterRPC.SystemLoad", &request, &response)
//	// do something
//	return &response, err
//}
//
//type RegisterSectorRequest struct {
//	Request
//	Sector
//}
//
//type RegisterSectorResponse struct {
//	Response
//}
//
//func (n *Node) RegisterSector(sector *Sector) error {
//	request := RegisterSectorRequest{requestArgs("SYSTEMLOAD", n), *sector}
//	response := RegisterSectorResponse{}
//	err := n.Send("ClusterRPC.RegisterSector", &request, &response)
//	// do something
//	return err
//	return nil
//}
//
//func (rpc *ClusterRPC) RegisterSector(request *RegisterSectorRequest, response *RegisterSectorResponse) error {
//	log.Println("Asked to register sector", request.Sector.ID)
//	return nil
//}
//
//type ClusterResource struct {
//	ParentID   ResourceID
//	ResourceID ResourceID
//	SectorID   SectorID
//	Name       string
//}
//
//func getClusterCommands() Command {
//	return Command{
//		"cluster",
//		[]CommandFlag{
//			{"na", "172.0.0.1:2000"},
//		},
//		nil,
//		[]Command{
//			{
//				"section",
//				[]CommandFlag{
//					{"name", "default",},
//				},
//				func(node *Node, ctx *Context, params map[string]*string) string {
//					var myslice ResourceID
//					fileSystem := NewResource(myslice, *params["name"], FileSystemResourceType)
//					log.Println(fileSystem)
//					return "#TODO this will create a new filesystem named " + *params["name"] // todo:
//				},
//				[]Command{
//					{
//						"list",
//						nil,
//						func(node *Node, ctx *Context, params map[string]*string) string {
//							return "todo: list the sections"
//						},
//						nil,
//					},
//				},
//			}, {
//				"list",
//				nil,
//				func(node *Node, ctx *Context, params map[string]*string) string {
//					return "#TODO this will list all known filesystems." // todo:
//				},
//				nil,
//			}, {
//				"load",
//				nil,
//				func(node *Node, ctx *Context, params map[string]*string) string {
//					resp, err := node.SystemLoad()
//					if (err != nil) {
//						logger.Debug(resp)
//						logger.Debug(err)
//					}
//					return fmt.Sprint(resp)
//				},
//				nil,
//			},
//		},
//	}
//}