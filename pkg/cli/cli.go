package cli

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"pando/pkg/server"
	"strings"
	"time"

	contextStore "pando/internal/pkg/context"
	"pando/internal/pkg/node"
	"pando/pkg/context"
	"pando/pkg/go/net/rpc"
	"pando/pkg/logger"
	"pando/pkg/node/ntypes"
)

type CommandParams = map[string]*string

type Connection struct {
	Addr ntypes.NodeAddr
	conn *rpc.Client
}

func (c *Connection) Connect() error {
	if c.conn == nil {
		conn, err := rpc.Dial("tcp", c.Addr)
		logger.ErrorIf(err)
		c.conn = conn
	}
	return nil
}

func (c *Connection) Send(serviceMethod string, args interface{}, reply interface{}) error {
	var connErr error
	for i := 0; i < 10; i++ {
		connErr = c.Connect()
		if connErr == nil {
			err := c.conn.Call(serviceMethod, args, reply)
			return err
		}
		time.Sleep(time.Second)
	}
	return connErr
}

type CommandFlag struct {
	name  string
	value string
}

type Command struct {
	command     string
	flags       []CommandFlag
	f           func(ctx context.Context, params CommandParams) string
	subCommands []Command
}

func (cmd *Command) Exec(ctx context.Context, flagStr string) string {
	flagSet := flag.NewFlagSet(cmd.command, flag.ContinueOnError)
	params := make(map[string]*string)
	for _, param := range cmd.flags {
		params[param.name] = flagSet.String(param.name, param.value, param.name)
	}
	_ = flagSet.Parse(strings.Fields(flagStr))

	if cmd.f != nil && len(flagSet.Args()) == 0 {
		//todo: or maybe flagStr has no remaing?
		return cmd.f(ctx, params)
	}
	nextCommand := flagSet.Arg(0)
	var nextFlags string
	if len(flagSet.Args()) == 0 {
		nextFlags = ""
	} else if len(flagSet.Args()) == 0 {
		nextFlags = flagSet.Args()[0]
	} else {
		nextFlags = strings.Join(flagSet.Args()[1:], " ")
	}
	for _, subCommand := range cmd.subCommands {
		if subCommand.command == nextCommand {
			return subCommand.Exec(ctx, nextFlags)
		}
	}
	log.Println("No Commands Found")
	return ""
}

func CLI() {
	//var err error
	//err = rpc.Register(&Server{})
	//
	//if err != nil {
	//	log.Fatal(err)
	//}
	//err = rpc.Register(&FileRPC{})
	//if err != nil {
	//	log.Fatal(err)
	//}

	//conf := config.LoadConfig()
	var ctx context.Context
	ctx = contextStore.NewContext()

	fmt.Println(ctx)

	seedNode, _ := node.NewNode(ctx.Config().Seed)
	fmt.Println(seedNode.Addr())
	ctx.AddNode(seedNode)

	rootCommand := getRootCommand()

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Pando Shell")
	fmt.Println("------------------------------")

	if len(flag.Args()) > 0 {
		runCmdArgs := strings.Join(flag.Args(), " ")
		fmt.Println(rootCommand.Exec(ctx, runCmdArgs))
		os.Exit(0)
	}
	for {
		fmt.Print("-> ")
		text, _ := reader.ReadString('\n')
		// convert CRLF to LF
		text = strings.Replace(text, "\n", "", -1)

		cmd := flag.NewFlagSet("Command", flag.ContinueOnError)
		cmd.Parse(strings.Fields(text))

		commandArgs := strings.Join(cmd.Args(), " ")
		fmt.Println(rootCommand.Exec(ctx, commandArgs))
	}

}

func getRootCommand() Command {
	return Command{
		command: "",
		flags: []CommandFlag{
			{"seed", "172.0.0.1:2000"},
		},
		f: nil,
		subCommands: []Command{
			Command{
				command: "config",
				f: func(ctx context.Context, params CommandParams) string {
					fmt.Println(ctx.Config())
					return ""
				},
				subCommands: nil,
			}, Command{
				command: "exit",
				f: func(ctx context.Context, params CommandParams) string {
					fmt.Println("Bye Bye :)")
					os.Exit(0)
					return ""
				},
				subCommands: []Command{
					Command{
						command: "list",
						f: func(ctx context.Context, params CommandParams) string {
							//ctx.nodes[0].List
							return "Done"
						},
						subCommands: nil,
					},
				},
			}, Command{
				command: "node",
				f: func(ctx context.Context, params CommandParams) string {
					return "from the node command."
				},
				subCommands: []Command{
					Command{
						command: "load",
						f: func(ctx context.Context, params CommandParams) string {
							//ctx.ListNodes()
							request := server.SystemLoadRequest{
								Request: server.Request{
									ID:      "todo:laskdjf",
									Time:    time.Time{},
									Request: "SYSTEMLOAD",
									Node:    nil,
								},
							}
							response := server.SystemLoadResponse{}
							ctx.Call("NodeRPC.SystemLoad", &request, &response)
							return fmt.Sprint("The load... is...", response.Load())
						},
						subCommands: nil,
					},
				},
			}, Command{
				command: "file",
				f: func(ctx context.Context, params CommandParams) string {
					return "from the file command."
				},
				subCommands: []Command{
					Command{
						command: "list",
						f: func(ctx context.Context, params CommandParams) string {
							return "list from the file command."
						},
						subCommands: nil,
					},
				},
			},
		},
	}
}
