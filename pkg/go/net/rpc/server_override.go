package rpc

import (
	"io"
	"log"
	"pando/pkg/context"
	"sync"
)

// Server represents an RPC Server.
type Server struct {
	serviceMap sync.Map   // map[string]*service
	reqLock    sync.Mutex // protects freeReq
	freeReq    *Request
	respLock   sync.Mutex // protects freeResp
	freeResp   *Response
	context    context.Context // pando context for finding node/sections
}

// NewServer returns a new Server.
func NewServer(context context.Context) *Server {
	server := Server{}
	server.context = context
	return &server
}

// DefaultServer is the default instance of *Server.
var DefaultServer = NewServer(nil)

// ServeCodec is like ServeConn but uses the specified codec to
// decode requests and encode responses.
func (server *Server) ServeCodec(codec ServerCodec) {
	sending := new(sync.Mutex)
	wg := new(sync.WaitGroup)
	for {
		service, mtype, req, argv, replyv, keepReading, err := server.readRequest(codec)
		log.Println("ContextStore", server.context)
		log.Println("RPC    ", service, )
		log.Println("MTYPE  ", mtype, )
		log.Println("REQ    ", req, )
		log.Println("ARGV   ", argv, )
		log.Println("REPLYV ", replyv, )
		if err != nil {
			if debugLog && err != io.EOF {
				log.Println("rpc:", err)
			}
			if !keepReading {
				break
			}
			// send a response if we actually managed to read a header.
			if req != nil {
				server.sendResponse(sending, req, invalidRequest, codec, err.Error())
				server.freeRequest(req)
			}
			continue
		}

		//if &argv != nil { // can't nil
		//	request, ok := argv.Interface().(pando.ResourceRequestChecker)
		//	if ok {
		//		log.Println("OKAY:", request.ResourceID())
		//	} else {
		//		log.Println("NOT OKAY")
		//	}
		//}

		wg.Add(1)
		go service.call(server, sending, wg, mtype, req, argv, replyv, codec)
	}
	// We've seen that there are no more requests.
	// Wait for responses to be sent before closing codec.
	wg.Wait()
	codec.Close()
}

// ServeRequest is like ServeCodec but synchronously serves a single request.
// It does not close the codec upon completion.
func (server *Server) ServeRequest(codec ServerCodec) error {
	sending := new(sync.Mutex)
	service, mtype, req, argv, replyv, keepReading, err := server.readRequest(codec)
	log.Println("RPC", service, mtype, req, argv, replyv, keepReading, err)
	if err != nil {
		if !keepReading {
			return err
		}
		// send a response if we actually managed to read a header.
		if req != nil {
			server.sendResponse(sending, req, invalidRequest, codec, err.Error())
			server.freeRequest(req)
		}
		return err
	}
	service.call(server, sending, nil, mtype, req, argv, replyv, codec)
	return nil
}
