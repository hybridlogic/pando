package ntypes

type NodeState = int
type NodeAddr = string
type NodeID = string

type stateList struct {
	Alive NodeState
	Dead  NodeState
}

// Enum for public use
var State = &stateList{
	Alive: 0,
	Dead:  1,
}