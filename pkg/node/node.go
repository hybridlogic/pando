package node

import (
	"pando/pkg/node/ntypes"
)

type Node interface {
	String() string
	ID() ntypes.NodeID
	Addr() ntypes.NodeAddr
	Connect() error
	Send(serviceMethod string, args interface{}, reply interface{}) error
}




//
////type nodeError struct {
////	err string
////}
////
////func (e *nodeError) Error() string {
////
////}
//
//type Node struct {
//	ID     NodeID
//	Addr   NodeAddr
//	State  NodeState
//	conn   *rpc.Client
//}
//
//func NewNode(addr NodeAddr) (*Node, error) {
//	node := Node{Addr: addr, ID: addr}
//	return &node, nil
//}
//
//func (n *Node) String() string {
//	return fmt.Sprint(n.ID, "@", n.Addr)
//}
//
//func (n *Node) Connect() error {
//	// resync if connection error?
//	if n.conn == nil {
//		conn, err := rpc.Dial("tcp", n.Addr)
//		if err != nil {
//			log.Println(err)
//			return err
//		}
//		n.conn = conn
//	}
//	return nil
//}
//
//func (n *Node) Send(serviceMethod string, args interface{}, reply interface{}) error {
//	// resync if connection error?
//	var connErr error
//	for i := 0; i < 10; i++ {
//		connErr = n.Connect()
//		if connErr == nil {
//			err := n.conn.Call(serviceMethod, args, reply)
//			return err
//		}
//		time.Sleep(time.Second) // a second can be a long time?
//	}
//	return connErr
//}

//func (n *Node) Register(reqNode *Node) (*RegisterResponse, error) {
//	request := NewRegisterRequest(reqNode)
//	response := RegisterResponse{}
//	err := n.Send("Server.Register", &request, &response)
//	// do something
//	return &response, err
//}
//
//func (n *Node) Heartbeat(reqNode *Node) (*AliveResponse, error) {
//	request := NewHeartbeatRequest(reqNode)
//	response := AliveResponse{}
//	err := n.Send("Server.Heartbeat", &request, &response)
//	//do something
//	return &response, err
//}
//
//
//type RegisterRequest struct {
//	Request
//}
//
//func NewRegisterRequest(reqNode *node.Node) *RegisterRequest {
//	request := RegisterRequest{
//		requestArgs("register", reqNode)}
//	return &request
//}
//
//func NewHeartbeatRequest(reqNode *node.Node) *HeartbeatRequest {
//	request := HeartbeatRequest{
//		requestArgs("heartbeat", reqNode),
//	}
//	return &request
//}
//
//type HeartbeatRequest struct {
//	Request
//}
//
//type NodeListRequest struct {
//	Request
//}