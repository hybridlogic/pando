package config

import (
	"flag"
	"fmt"
	"github.com/imdario/mergo"
	"os"
	"strconv"
	"strings"

	"pando/pkg/logger"
)

type Config struct {
	Seed         string
	Advertise    string
	Address      string
	Port         int
	StoragePaths []string
}

func LoadConfig() *Config {
	logger.Print("Loading Config...")
	config := Config{}
	configEnv := Config{
		Advertise:    getEnvStr("PANDO_ADVERTISE", "127.0.0.1:2000"),
		Seed:         getEnvStr("PANDO_SEED", "127.0.0.1:2000"),
		Address:      getEnvStr("PANDO_ADDRESS", "0.0.0.0:2000"),
		Port:         getEnvInt("PANDO_PORT", 2000),
		StoragePaths: strings.Split(getEnvStr("PANDO_STORAGE_PATHS", "/var/pando"), ","),
	}
	configFlags := configFromFlags()
	logger.ErrorIf(mergo.Merge(&config, configEnv))
	logger.ErrorIf(mergo.Merge(&config, configFlags))
	return &config
}

func configFromFlags() *Config {
	var advertise string
	var seed string
	var address string
	var port int
	flag.StringVar(&advertise, "advertise", "", "Advertise address")
	flag.StringVar(&seed, "seed", "", "Seed address")
	flag.StringVar(&address, "address", "", "Listen address")
	flag.IntVar(&port, "port", 2000, "Listen port")
	flag.Parse()
	config := Config{
		Advertise: advertise,
		Seed:      seed,
		Address:   address,
		Port:      port,
	}
	return &config
}

func getEnvStr(key string, fallback string) string {
	value, has := os.LookupEnv(key)
	if has {
		return value
	}
	return fallback
}

func getEnvInt(key string, fallback int) int {
	value, has := os.LookupEnv(key)
	if has {
		i, err := strconv.Atoi(value)
		if err != nil {
			fmt.Println(err.Error())
		}
		return i
	}
	return fallback
}
