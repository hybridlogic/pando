package logger

import (
	"encoding/json"
	"log"
)

type LogLevel int

var logLevel LogLevel = 0

const (
	DEBUG LogLevel = 0
	INFO  LogLevel = 2
	WARN  LogLevel = 4
	ERROR LogLevel = 6
	FATAL LogLevel = 8
)

func label(level LogLevel) string {
	switch level {
	case DEBUG:
		return "DEBUG"
	case INFO:
		return "INFO"
	case WARN:
		return "WARN"
	case ERROR:
		return "ERROR"
	case FATAL:
		return "FATAL"
	default:
		return "UNKNOWN"
	}
}

func write(l LogLevel, v ...interface{}) {
	if l >= logLevel {
		//out := append(label(l), v...)
		v = append([]interface{}{label(l), "|"}, v...)
		log.Println(v...)
	}
}

func fatal(v ...interface{}) {
	v = append([]interface{}{label(FATAL), "|"}, v...)
	log.Fatalln(v...)
}

func SetLogLevel(l LogLevel) {
	logLevel = l
}

func Log(l LogLevel, v ...interface{}) {
	write(l, v...)
}

func Json(l LogLevel, v ...interface{}) {
	var j []interface{}
	for i := range v {
		s, err := json.Marshal(i)
		if err != nil {
			j = append(j, i)
			continue
		}
		j = append(j, string(s))
	}
	write(l, j...)
}

func Print(v ...interface{}) {
	write(INFO, v...)
}

func Debug(v ...interface{}) {
	write(DEBUG, v...)
}

func Info(v ...interface{}) {
	write(INFO, v...)
}

func Warn(v ...interface{}) {
	write(WARN, v...)
}

func Error(v ...interface{}) {
	write(ERROR, v...)
}

func Fatal(v ...interface{}) {
	fatal(v...)
}

func ErrorIf(err error) {
	if err != nil {
		write(ERROR, err)
	}
}
func FatalIf(err error) {
	if err != nil {
		fatal(err)
	}
}
