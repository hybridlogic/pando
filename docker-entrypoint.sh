#!/bin/bash

#mkdir /tmp/go-mod
inotifywait -h
if [ "$?" == "127" ]; then
  echo "installing inotifywait"
  apt update
  apt install -y inotify-tools
else
    echo "inotifywait is already installed"
fi
#exit 0
while true; do
  echo "Running: $@"
  $@ &
  PID=$!
#  echo "Running under $PID or $(pgrep server)"
  inotifywait -r -e modify .
  kill -9 $PID
#  echo "Still running? $(pgrep server)"
  kill -9 $(pgrep pando)
  sleep 3
done