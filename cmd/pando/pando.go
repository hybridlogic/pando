package main

import (
	"math/rand"
	"pando/pkg/context"
	"time"

	contextStore "pando/internal/pkg/context"
	"pando/pkg/logger"
	"pando/pkg/server"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	logger.Info("----------------")
	var ctx context.Context
	ctx = contextStore.NewContext()

	// Start the server
	srv := server.NewServer(ctx)
	srv.Serve()

	// find other nodes
	time.Sleep(time.Second * time.Duration(rand.Intn(10-1+1)+1))
	//ctx.FindNodes(conf.Seed)

	// todo: heartbeats
	//for {
	//	time.Sleep(time.Second * time.Duration(rand.Intn(60-30+1)+30))
	//	logger.Debug("Sending Heartbeats")
	//	for _, node := range ctx.Nodes() {
	//		node.Heartbeat(ctx.ContextNode())
	//	}
	//}

	// todo: make an exit channel to quit. on context of server?
	logger.Debug("I'm going to bed for a minute")
	//time.Sleep(time.Minute)
	exitChan := make(chan bool)
	//go func() {
	//	time.Sleep(time.Second)
	//	exitChan <- false
	//}()
	_ = <-exitChan
	logger.Info("Bye Bye")
}
